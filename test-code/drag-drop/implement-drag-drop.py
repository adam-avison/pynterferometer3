from bokeh.plotting import figure, output_file, show, Column
from bokeh.models import DataTable, TableColumn, PointDrawTool, ColumnDataSource
import sys
sys.path.append('../../pynterferometer3-app/')

from arrayConfigs.antPositions import *


# -- Image dimensions
im_dim = 500
num_ants = 10 

orig_x = spiral_x[:num_ants]
orig_y = spiral_y[:num_ants]

arraydata = {'x-array': orig_x, 'y-array': orig_y}
sourcearr = ColumnDataSource(data = arraydata)

parr = figure(height = im_dim, width = im_dim, title="Array positions", toolbar_location=None, match_aspect=True)
parr_dnd = parr.circle(x='x-array', y='y-array', source=sourcearr, size=20, color="navy")

draw_tool = PointDrawTool(renderers=[parr_dnd], empty_value='black')
parr.add_tools(draw_tool)
parr.toolbar.active_tap = draw_tool


show(parr)

