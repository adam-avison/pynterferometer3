import cv2
import numpy as np

""" Method borrowed from: https://www.geeksforgeeks.org/circle-detection-using-opencv-python/"""

#--- To Do link to webcam
image  = cv2.imread('test_img/test1.png', cv2.IMREAD_COLOR)
x_siz, y_siz, z_siz = image.shape

#--- Grayscale
im_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) 

im_gray_blur = cv2.blur(im_gray, (6, 6)) 
#--- Hough transform on the blurred image. 
found_circles = cv2.HoughCircles(im_gray_blur,  
                   cv2.HOUGH_GRADIENT, 1, 20, param1 = 70, 
               param2 = 30, minRadius = 1, maxRadius = 40) 

if found_circles is not None:
    found_circles = np.uint16(np.around(found_circles)) 
    print(found_circles)
    # for pt in found_circles[0, :]: 
    #     a, b, r = pt[0], pt[1], pt[2] 
  
    #     # Draw the circumference of the circle. 
    #     cv2.circle(image, (a, b), r, (0, 255, 0), 2) 
  
    #     # Draw a small circle (of radius 1) to show the center. 
    #     cv2.circle(image, (a, b), 1, (0, 0, 255), 3) 
    #     cv2.imshow("Found Circle", image) 
    #     cv2.waitKey(0) 
    
    #--- Transpose to and get X and Y coords
    x = found_circles[0][:].T[0]/x_siz
    y = found_circles[0][:].T[1]/y_siz

    print(x,y)