import os
#import sys
import numpy as np
from scipy import fftpack
from bokeh.plotting import figure
from bokeh.layouts import column, row, layout, Spacer
from bokeh.models import ColumnDataSource, Div, Slider, RadioButtonGroup, InlineStyleSheet, PointDrawTool #Column, 
from bokeh.io import curdoc
from bokeh.models.widgets import DataTable, TableColumn#, HTMLTemplateFormatter, DateFormatter, 
#from bokeh.events import PanEnd, PointEvent
import matplotlib.image as mpimg
#import matplotlib.pyplot as plt
#from functools import partial  # needed to extend the Bokeh event handling to pass an argument value
from templates.inlineStyle import *

from helpers.arraySelect import *
from helpers.imageSelect import *

# ---- Import external data
from arrayConfigs import antPositions

"""================= WIP ======================"""

""" Pynterferometer Version 3 """
""" A.Avison                  """
""" Created: 09-DEC-2021      """
""" 1st Release: XX-XXX-2024  """


""" Assumptions/shortcuts:                          """
""" 1. Ignoring w-projection                        """
""" 2. Assume Northern source Dec= 0 -> 90 deg      """

""" To run,  from the directory above pynterferometer3-app invoke : """
""" bokeh serve --show pynterferometer3-app """
# =================================#
# ==== CLASSES ====================#
# =================================#

# ----- Array Properties ---------#
# Antenna positions, uv-coverage  #
# --------------------------------#

class ArrayUvProps:
    """Class to deal with array properties and uv-coverage"""

    def __init__(self, x: np.array, y: np.array, t: float, wavel: float, sin_dec: float, arrayscale: float):
        self.x = x  # -- Antenna x position unitless, normalised to 1 at max
        self.y = y  # -- Antenna y position unitless, normalised to 1 at max
        self.arrayscale = arrayscale*1000.  # -- Array linear max scale in unit m (entered as km, 1000. gets to m)
        self.t = t  # -- time on source in hours
        self.wavel = wavel  # -- observing wavelength in metres
        self.sin_dec = sin_dec  # -- Sin of the observatory declination

    def baselinesep(self):
        # -- calculate separations of baselines
        lx = np.zeros((len(self.x), len(self.x)))   # -- Baseline separations in x, m
        ly = np.zeros((len(self.y), len(self.x)))   # -- Baseline separations in y, m

        for i in range(len(self.x)):
            for j in range(len(self.y)):
                lx[i, j] = (self.x[i]*self.arrayscale - self.x[j]*self.arrayscale)  # -- in m
                ly[i, j] = (self.y[i]*self.arrayscale - self.y[j]*self.arrayscale)  # -- in m

        return lx, ly

    def snapshotuv(self):
        # -- calculate snapshot uv-coverage
        lx, ly = self.baselinesep()

        u = (np.ceil(ly/self.wavel))                         # -- u values for HA = 0, unitless (lambda)
        v = (np.ceil((-1. * lx * self.sin_dec) / self.wavel))  # -- v values for HA = 0, unitless (lambda)

        re_u = np.reshape(u, (len(self.x)**2), order='F')    # -- reshape u into linear form, in lambda
        re_v = np.reshape(v, (len(self.y)**2), order='F')    # -- reshape v into linear form, in lambda

        return re_u, re_v

    def earthrotuv(self):
        #--- calculate earth rotation uv-coverage for time t
        #--- time t is in hours, so *15. to get angle in degrees.
        #--- time t has a maximum limiyt of 12h """

        lx, ly = self.baselinesep()

        re_u = 0
        re_v = 0

        for ha in np.arange(0.0, (self.t+1.0)*15.0, 15.0):
            sin_ha = np.sin((ha * np.pi) / 180.)
            cos_ha = np.cos((ha * np.pi) / 180.)

            u = (np.ceil((((lx * sin_ha) + (ly * cos_ha)) / self.wavel)))  # -- u values including ha
            v = (np.ceil(((-1. * lx * self.sin_dec * cos_ha) + (ly * self.sin_dec * sin_ha)) / self.wavel))  # v values

            re_u = np.append(re_u, np.reshape(u, (len(self.x) ** 2), order='F'))  # -- reshape u into linear form, in lambda
            re_v = np.append(re_v, np.reshape(v, (len(self.y) ** 2), order='F'))  # -- reshape v into linear form, in lambda

        return re_u, re_v

# ---- Sky image Class------------#
# The sky image is the 'true sky' #
# --------------------------------#


class SkyImage:
    """Class to deal with Sky image data"""
    def __init__(self, skyimname: str, skysize: float):
        self.skyimname = skyimname  # -  Sky image file name
        self.skysize = skysize  # -  Sky image max extent in arcseconds, user/coder defined

    def pixeldata(self):
        return np.rot90(mpimg.imread(self.skyimname), 2)  # -- The Rot 90 is to rotate the image the right way up

    def ampphase(self):
        """ extract the sky images amplitude and phases by taking an FFT"""
        inimg = self.pixeldata()
        j=2
        fftimg = np.fft.fft2(inimg[:, :, j])
        fft_sky1 = fftpack.fftshift(fftimg) # --  this shifts the FFT to get the origin correct
        return np.abs(fft_sky1), np.angle(fft_sky1)# amps, phases#

    def skypixelscales(self):
        """ Calculate image pixel size in lambda units"""
        inimg = mpimg.imread(self.skyimname)
        no_xpix = inimg.shape[0]
        no_ypix = inimg.shape[1]
        oneasecbaseline = (180.0 / np.pi) * 3600.  # - Conversion factor to take arcsec to lambda
        skyscale_x = (self.skysize * oneasecbaseline) / no_xpix  # - x-axis pixels size in lambda
        skyscale_y = (self.skysize * oneasecbaseline) / no_ypix  # - y-axis pixels size in lambda
        return skyscale_x, skyscale_y


# ----- Recovered image ----------#
# The recovered image is the      #
# 'true sky' as observed by the   #
# interferometer. The 'dirty'     #
# image in interferometer jargon  #
# --------------------------------#

class RecoveredImage:
    """Class to deal with recovered image data"""
    def __init__(self, recimname: str, amp: np.array, phase: np.array, u: np.array, v: np.array, pixscale_x: float,
                pixscale_y: float):

        self.imName = recimname         # -- Recovered image name
        self.a = amp                    # -- FFT image amplitude in pixel units
        self.p = phase                  # -- FFT image phase in pixel units
        self.u = u                      # -- u-values in lamda
        self.v = v                      # -- v-values in lamda
        self.pixscale_x = pixscale_x    # -- x-axis pixels size in lambda
        self.pixscale_y = pixscale_y    # -- y-axis pixels size in lambda

    def uvpixelmatrix(self):
        """Convert uv-coverage in real units to pixel"""
        # -- rescale to image scale
        uimg = (self.u) / self.pixscale_x     # - u in pixels
        vimg = (self.v) / self.pixscale_y     # - v in pixels

        # -- create an empty matrix same size as in image for multiplying with fft of true sky image
        x = self.a.shape[0]
        y = self.a.shape[1]
        obs_uv_matrix = np.zeros((x, y))
        u_mid = int((x / 2.0) - 1)  # -- An offset for the u, in pixels (as uv- are pos and neg).
        v_mid = int((y / 2.0) - 1)  # -- An offset for the v, in pixels

        for k in range(len(uimg)):  # -  WARNING THIS IS ONLY WORKS FOR SQUARE IMAGES
            int_u = int(uimg[k] + u_mid)
            int_v = int(vimg[k] + v_mid)
            if 0 <= int_u < self.a.shape[0] and 0 <= int_v < self.a.shape[1]:
                obs_uv_matrix[int_v, int_u] = 1.0  # -- array indicies are in pixels
        obs_uv_matrix[v_mid, u_mid] = 0.0
        return obs_uv_matrix

    def observesky(self):
        """ Impose the uv-coverage on the sky A&P """
        obsuv = self.uvpixelmatrix()
        uv_a = self.a * obsuv
        uv_p = self.p * obsuv
        observedsky = np.abs(np.fft.ifft2(np.fft.fftshift(uv_a*np.exp(1j*uv_p))))
        return observedsky

def update_in_image(attrname, old, new):
    """
    Callback for updating the data when a new Sky image is selected.
    """
    inimageidx = image_button_group.active
    image_option = ["Dog", "Protocluster", "Photo"]
    useimage = imagedir + "/" + selectImage(image_option[inimageidx])
    os.system("cp "+useimage+" "+imagedir+"/current_img.png") #--- bit of a fudgy hack

    # -- recalculate the Sky information amps, phases and pixel sizes.
    useimage = imagedir+"/current_img.png" 
    skyimg = SkyImage(useimage, 2.0)
    am, ph = skyimg.ampphase()  # -- re calc amps and phases
    pixl_x, pixl_y = skyimg.skypixelscales()
    pixeldat = skyimg.pixeldata()
    sourcesky.data = {'skyimage': [pixeldat[:, :, 1]]} # -update sky image data in Bokeh image

    # -- update the recovered sky based on the updated current inputs.
    recimg = RecoveredImage(useimage, am, ph, sourceuv.data['u-values'], sourceuv.data['v-values'], pixl_x, pixl_y)
    obsky = recimg.observesky()
    sourcerecsky.data = {'recimage': [obsky[:, :]]}

def update_obs_setup(attrname, old, new):
    """
    Callback for updating the data when any change is made to the observing setup
    e.g. array shape, freq, time, declination etc.
    """
    arrayidx = array_button_group.active
    array_option = ["Spiral", "Y-shape", "Line", "Camera"]
    if array_option[arrayidx] == "Camera" and os.path.exists("pynterferometer3-app/arrayConfigs/array_image.png"):
        print("image already taken")
    else:
        arr_x, arr_y = selectArray(array_option[arrayidx])

    num_ants = range_num_ants.value

    orig_x = arr_x[:num_ants]
    orig_y = arr_y[:num_ants]
    
    freq = range_freq.value  # -- get the current frequency value.
    time = range_time.value  # -- get the current time value.
    dec = range_dec.value  # -- get the current time value.
    sin_dec = np.sin(np.radians(dec))  # -- take the sin of the declination for easier calculation
    obswave = 299792458.0/(freq*1.0e9)  # -- convert frequency to wavelength

    # -- replot array
    sourcearr.data = {'x-array': orig_x, 'y-array': orig_y}

    # -- recalculate the Sky information amps, phases and pixel sizes.
    useimage = imagedir+"/current_img.png"
    skyimg = SkyImage(useimage, 2.0)
    am, ph = skyimg.ampphase()  # -- re calc amps and phases
    pixl_x, pixl_y = skyimg.skypixelscales()
    pixeldat = skyimg.pixeldata()
    sourcesky.data = {'skyimage': [pixeldat[:, :, 1]]} # -update sky image data in Bokeh image

    # -- recalculate the UV properties for updated current inputs.
    arrprop = ArrayUvProps(orig_x, orig_y, time, obswave, sin_dec, arrayscale)
    current_uvpoints = arrprop.earthrotuv()
    sourceuv.data = {'u-values': current_uvpoints[0], 'v-values': current_uvpoints[1]}

    #-- update the recovered sky based on the updated current inputs.
    recimg = RecoveredImage(useimage, am, ph, current_uvpoints[0], current_uvpoints[1], pixl_x, pixl_y)
    obsky = recimg.observesky()
    sourcerecsky.data = {'recimage': [obsky[:, :]]}

def update_ant_pos(attrname, old, new):
    """
    Call back for when dragging and dropping antennas around
    """
    num_ants = range_num_ants.value

    freq = range_freq.value  # -- get the current frequency value.
    time = range_time.value  # -- get the current time value.
    dec = range_dec.value  # -- get the current time value.
    sin_dec = np.sin(np.radians(dec))  # -- take the sin of the declination for easier calculation
    obswave = 299792458.0/(freq*1.0e9)  # -- convert frequency to wavelength

    orig_x = sourcearr.data['x-array'][:num_ants]

    orig_y = sourcearr.data['y-array'][:num_ants]
    # -- recalculate the Sky information amps, phases and pixel sizes.
    useimage = imagedir+"/current_img.png"
    skyimg = SkyImage(useimage, 2.0)
    am, ph = skyimg.ampphase()  # -- re calc amps and phases
    pixl_x, pixl_y = skyimg.skypixelscales()
    pixeldat = skyimg.pixeldata()
    sourcesky.data = {'skyimage': [pixeldat[:, :, 1]]} # -update sky image data in Bokeh image

    # -- recalculate the UV properties for updated current inputs.
    arrprop = ArrayUvProps(orig_x, orig_y, time, obswave, sin_dec, arrayscale)
    current_uvpoints = arrprop.earthrotuv()
    sourceuv.data = {'u-values': current_uvpoints[0], 'v-values': current_uvpoints[1]}

    #-- update the recovered sky based on the updated current inputs.
    recimg = RecoveredImage(useimage, am, ph, current_uvpoints[0], current_uvpoints[1], pixl_x, pixl_y)
    obsky = recimg.observesky()
    sourcerecsky.data = {'recimage': [obsky[:, :]]}


# ===================================#
# ========= CODE PROPER =============#
# ===================================#

# -- INITIATE
# -- Get Array positions
orig_x = antPositions.spiral_x
orig_y = antPositions.spiral_y
declination = 45.0     # - Observatory declination in degrees
sindec = np.sin(np.radians(declination))
arrayscale = 1.0    # - Linear size of array area, in km
obswavel = 4.47e-2    # - Observing wavelength, in m

# -- Image dimensions
im_dim = 350

# -- setup sky image
imagearr = ["Dog.png", "ProtoC.png"]
imidx = 0
imagedir = "pynterferometer3-app/skyImages"
useimage = imagedir+"/"+imagearr[imidx]
os.system("cp "+useimage+" "+imagedir+"/current_img.png") #--- bit of a fudgy hack
useimage = imagedir+"/current_img.png" 

a = SkyImage(useimage, 2.0)
amp, phase = a.ampphase()
pixlambda_x, pixlambda_y = a.skypixelscales()
pixeldata = a.pixeldata()

b = ArrayUvProps(orig_x, orig_y, 2.0, obswavel, sindec, arrayscale)
uvpoints = b.snapshotuv()

arraydata = {'x-array': orig_x, 'y-array': orig_y}
sourcearr = ColumnDataSource(data = arraydata)
uvdata = {'u-values': uvpoints[0], 'v-values': uvpoints[1]}
sourceuv = ColumnDataSource(data = uvdata)
sourcesky = ColumnDataSource({'skyimage': [pixeldata[:, :, 1]]})

c = RecoveredImage(pixeldata, amp, phase, uvpoints[0], uvpoints[1], pixlambda_x, pixlambda_y)
obssky = c.observesky()
sourcerecsky = ColumnDataSource({'recimage': [obssky[:, :]]})

# -- Create & populate the array plot space
parr = figure(height = im_dim, width = im_dim, title="Array positions", toolbar_location=None, match_aspect=True)
parr_dnd = parr.circle(x='x-array', y='y-array', source=sourcearr, size=8, color="navy")
draw_tool = PointDrawTool(renderers=[parr_dnd], empty_value='black')
parr.add_tools(draw_tool)
parr.toolbar.active_tap = draw_tool
sourcearr.selected.on_change('indices',update_ant_pos)

# -- Create & populate the uvplot plot space
puv = figure(height = im_dim, width = im_dim, title="uv-coverage", toolbar_location=None, match_aspect=True)
puv.circle(x='u-values', y='v-values', source=sourceuv, size=8, color="navy")
puv.circle(x=0, y=0, color='white', size=8)

# -- Create & populate the sky image plot space
psky = figure(height= im_dim , width = im_dim, title="Real Sky", toolbar_location=None, match_aspect=True)
psky.image(image='skyimage', x=0, y=0, dw=1, dh=1, palette="Viridis256", source=sourcesky)
psky.axis.visible = False  # -- Hide axes
psky.grid.visible = False  # -- Hide grid
psky.outline_line_color = None  # -- Hide border

# -- Create & populate the recovered sky image plot space
puvsky = figure(height = im_dim, width = im_dim, title="Observed Sky", toolbar_location=None, match_aspect=True)
puvsky.image(image='recimage', x=0, y=0, dw=1, dh=1, palette="Viridis256", source=sourcerecsky)
puvsky.axis.visible = False  # -- Hide axes.
puvsky.grid.visible = False  # -- Hide axes
puvsky.outline_line_color = None  # -- Hide border


# ====== Bokeh Webpage =============#
# ==== Inline StyleSheets
#--- Button Style
button_instyle = InlineStyleSheet(css=btn_style)#".bk-btn-default {font-size: 20px !important}")
#--- Slider Style
slider_instyle = InlineStyleSheet(css=sldr_style)
#--- Label Div Style
label_div_instyle = InlineStyleSheet(css=label_style)

# -- Put in some sliders etc and recover their values

range_freq = Slider(
    title="Frequency [GHz]", # a title to display above the slider
    start=1.0,  # set the minimum value for the slider
    end=25.0,  # set the maximum value for the slider
    step=0.5,  # increments for the slider
    value=6.7,  # initial values for slider
    stylesheets=[slider_instyle]
    )

range_time = Slider(
    title="On source time [Hours]", # a title to display above the slider
    start=1,  # set the minimum value for the slider
    end=12,  # set the maximum value for the slider
    step=1,  # increments for the slider
    value=1,  # initial values for slider
    stylesheets=[slider_instyle]
    )

range_dec = Slider(
    title="Source Declination [deg]", # a title to display above the slider
    start=0,  # set the minimum value for the slider
    end=90,  # set the maximum value for the slider
    step=1,  # increments for the slider
    value=45,  # initial values for slider
    stylesheets=[slider_instyle]
    )

range_num_ants = Slider(
    title="Number of antennas", # a title to display above the slider
    start=1,  # set the minimum value for the slider
    end=50,  # set the maximum value for the slider
    step=1,  # increments for the slider
    value=45,  # initial values for slider
    stylesheets=[slider_instyle]
    )


# -- Input image buttons
image_button_group = RadioButtonGroup(
        labels=["Dog", "Protocluster", "Photo"], active=0, height = 70, width = 300, stylesheets=[button_instyle])
image_button_group.on_change('active', update_in_image) 

# -- Input array buttons
array_button_group = RadioButtonGroup(
        labels = ["Spiral", "Y-shape", "Line", "Camera"], active=0, height = 70, width = 300, 
        stylesheets=[button_instyle])
array_button_group.on_change('active', update_obs_setup)

# --- Set the on_change events for the sliders
range_freq.on_change('value', update_obs_setup)
range_time.on_change('value', update_obs_setup)
range_dec.on_change('value', update_obs_setup)
range_num_ants.on_change('value', update_obs_setup)

# --- IMPLEMENT THE LAYOUT -----#
#--- Header ---#
header =  Div(text="""Pynterferometer 3""", height=50, 
            align='start', name='header_div', css_classes=['header'])

#--- Footer ---#
footer =  Div(text="""<br/> 2024 | <a href="https://gitlab.com/adam-avison">Adam Avison</a> """, height=50,
            align='start', name='footer_div', css_classes=['footer']) 

#-- Control labels ---#
image_control_label = Div(text="""Image Controls""", stylesheets=[label_div_instyle], name='image_control_label_div')
array_control_label = Div(text="""Array Controls""", stylesheets=[label_div_instyle])
obs_control_label = Div(text="""Observing Controls""", stylesheets=[label_div_instyle])


sliders = column(obs_control_label, range_freq, range_time, range_dec, range_num_ants)
buttons = column(image_control_label, image_button_group, array_control_label, array_button_group)
plots = row(parr, psky, puv, puvsky)
slide_butt = row(children = [Spacer(width=300, height=1), buttons, Spacer(width=50, height=1), sliders], 
                sizing_mode='stretch_width')
cols = column(plots, Spacer(width=1, height=30), slide_butt, Spacer(width=1, height=30))
dash  = layout(children=[cols], name='dash')

curdoc().add_root(header)
curdoc().add_root(dash)
curdoc().add_root(footer)