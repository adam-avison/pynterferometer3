import cv2
from time import sleep

#!!!!!!!!! NEEEDS FINISHING !!!!!!!!!!!!!

"""TO BE RUN BEFORE FIRST USING PYNTERFEROMETER-3 ON A NEW SYSTEM"""
"""DETECTS AVAILABLE WEBCAMS AND MAKES A QUICK IMAGE SO YOU KNOW """
""" WHICH IS WHICH """

def detect_webcams(camera_range):
    # detect all connected webcams
    valid_cams = []
    for i in range(camera_range):
        print(i)
        cap = cv2.VideoCapture(i)
        sleep(2)
        if cap is None or not cap.isOpened():
            print('Unable to open video source: ', i)
        else:
            valid_cams.append(i)
            s, img = cap.read()
            if s: 
                cv2.imwrite("test_image_webcam"+str(i)+".jpg",img) #save image
                cap.release()

    return valid_cams

camera_range = 5 #--- number of cameras to test if they exist
detect_webcams(camera_range)
