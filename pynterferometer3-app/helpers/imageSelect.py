from helpers.imageCaptureUser import image_capture_user
from setup.setupWebcams import *
from time import sleep

def selectImage(image_option):
    """ This could possibly make use of enums"""
    if image_option == "Dog":
        use_image = "Dog.png"
    elif image_option == "Protocluster":
        use_image = "ProtoC.png"
    elif image_option == "Photo":
        image_capture_user(user_facing_camera_idx)
        use_image = "user_image.png"

    return use_image

print(selectImage)