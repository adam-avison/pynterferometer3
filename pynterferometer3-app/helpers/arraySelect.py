# ---- Import external data
from setup.setupWebcams import *
from arrayConfigs.antPositions import *
from helpers.imageCaptureArrayConfig import image_capture_array_config

def selectArray(array_option):
    """ This could possibly make use of enums"""
    if array_option == "Spiral":
        arr_x = spiral_x
        arr_y = spiral_y
    elif array_option == "Y-shape":
        arr_x = yshape_x
        arr_y = yshape_y
    elif array_option == "Line":
        arr_x = line_x
        arr_y = line_y
    elif array_option == "Camera":
        arr_x, arr_y = image_capture_array_config(array_camera_idx)

    return arr_x, arr_y