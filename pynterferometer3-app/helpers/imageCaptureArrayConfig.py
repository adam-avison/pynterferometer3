import cv2
import numpy as np
from time import sleep
""" Basic Method borrowed from: https://www.geeksforgeeks.org/circle-detection-using-opencv-python/"""

def image_capture_array_config(cam_idx):
    cam = cv2.VideoCapture(cam_idx)   # 0 -> index of camera
    sleep(1) #--- need this to get a decently exposed image
    s, img = cam.read()
    if s:    # frame captured without any errors
        cv2.imwrite("pynterferometer3-app/arrayConfigs/array_image.png",img) #save image
        # cv2.imwrite("this_img.png",img)
        cam.release()

        image  = cv2.imread("pynterferometer3-app/arrayConfigs/array_image.png", cv2.IMREAD_COLOR)
        # image  = cv2.imread("this_img.png", cv2.IMREAD_COLOR)
        x_siz, y_siz, z_siz = image.shape

        #--- Grayscale
        im_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) 

        im_gray_blur = cv2.blur(im_gray, (4, 4)) 
        #--- Hough transform on the blurred image. 
        found_circles = cv2.HoughCircles(im_gray_blur,  
                        cv2.HOUGH_GRADIENT, 1, 200, param1 = 80, 
                        param2 = 30, minRadius = 50, maxRadius = 200) 
    

        if found_circles is not None:
            found_circles = np.uint16(np.around(found_circles)) 

            # for pt in found_circles[0, :]: 
            #     a, b, r = pt[0], pt[1], pt[2] 
            #     # Draw the circumference of the circle. 
            #     cv2.circle(image, (a, b), r, (0, 255, 0), 2) 
            #     # Draw a small circle (of radius 1) to show the center. 
            #     cv2.circle(image, (a, b), 1, (0, 0, 255), 3) 
            #     cv2.imshow("Detected Circle", image) 
            # cv2.waitKey(0)
                
            #--- Transpose to and get X and Y coords
            x = found_circles[0][:].T[0]/x_siz
            y = found_circles[0][:].T[1]/y_siz
        else:
            x = 0
            y = 0

    return x, y

# print(image_capture_array_config(0))