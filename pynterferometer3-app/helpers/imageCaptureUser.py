import cv2
import os
from time import sleep

def image_capture_user(cam_idx):
    os.system('rm pynterferometer3-app/skyImages/user_image.png')
    cam = cv2.VideoCapture(cam_idx)   # 0 -> index of camera
    sleep(1) #--- need this to get a decently exposed image
    s, img = cam.read()
    if s:    # frame captured without any errors
        height,width,alph = img.shape
        cropped_img = img[int((height/2.0)-400):int((height/2.0)+400), int((width/2.0)-400):int((width/2.0)+400), :]
        # cropped_img_gray = cv2.cvtColor(cropped_img, cv2.COLOR_BGR2GRAY) 
        cv2.imwrite("pynterferometer3-app/skyImages/user_image.png",cropped_img) #save image
        cam.release()



# print(image_capture_user(1))