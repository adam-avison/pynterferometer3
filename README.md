# Pynterferometer3

## About
A version 3.0 for the Pynterferometer.

This version aims to up-date the much neglected Pynterferometer to Python 3.x, utilise Bohek as the front-end (over TK from previous versions), and generally improve the code. (Version 1.0 was written when one of the original creators had less than a years python under their belt).

### Purpose
The primary purpose of the Pynterferometer is to allow a 'hands-on' toy interferometer for use in Public outreach events.

### History
The Pynterferometer was created in 2011-2012 by Adam Avison and Samuel George (then of the UK ALMA Regional Centre and Universtion of Cambridge respectively), for the annual Royal Society Summer Science exhibition. The original version was used at this exhibition and a _lot_ of Public Outreach events in the years that followed. The original release and its reception were documented in this publication, https://iopscience.iop.org/article/10.1088/0143-0807/34/1/7.

A version 2.0 with slightly advanced features was released by Adam Avison in 2015 for the European Radio Interferometry School held at ESO.

Since then, the pynterferometer has been a bit negelected. 

Version 2.0 (in Python 2) can be downloaded here: https://www.jb.man.ac.uk/pynterferometer/

---

## Usage

### Run the Pynterferometer
First check the Python requirements in the file `requirements.txt`
1. Clone this repo
2. From the directory above the ```pynterferometer3-app``` directory type command ```bokeh serve --show pynterferometer3-app```
3. This should boot up an instance in your default browser.
4. Enjoy.

### Current functionality
1. User can select from 2 pre-set images plus take an image with the on board webcam
2. User can select between 3 pre-defined array shapes, or if an external webcam is included image a toy array (this is very experimental!)
3. Change integration time in intervals of 1 hour. Effects on image can be seen whislt changing.
4. Change the observing frequency over a wide range of values, effects on image can be seen whilst changing.
5. Change the declination of the target, currently assumes a northern hemisphere array.
6. Drag individual antennas around.

### Adam's To Do list
1. ~~Add ability to add/remove antennas.~~ Done as a slider.
2. ~~Add ability to move antennas around in the array.~~
    1. This is slightly buggy and needs some more attention.
3. ~~Add User Image capture.~~
4. Use an external camera + toy array to generate antenna positions within the Pynterferometer. <- WORK IN PROGRESS
    1. Refine the method and set up of the physical array
    2. Add in some logic to prevent users using the antenna number slider when using an external camera.
    3. Add in all the error handling
5. Complete the setup scripts and documentation for these.
6. add Units to the array positions and uv-coverage plots
7. Think about showing the dirty beam (point spread function)

---

## Example Exercises

### Look at the fringes.
1. Use the Antenna slider to reduce the number of antenna to 2.
2. Observe the width of the fringes (light and dark bands) recovered in the output image.
3. Use your mouse to drag one of antennas further away from the other one.
4. How does this change the width of the fringes?
5. Now drag it nearer to the other antenna.
6. How does this change the width of the fringes?
7. Consider how these fringe widths relate to spatial scales and begin adding more antennas back into the array.

### The shape of your array.
1. Use the Antenna slider to reduce the number of antenna to around 20. (This highlights the effects more than 50+ antennas do.)
2. Use the array selector buttons to change between the Spiral, Y-shaped and line array configurations.
3. How do the different arrays change what you see in the recovered image.
